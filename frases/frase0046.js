<!--
document.write ("<b><i>&quot;Nenhuma uni&atilde;o se manter&aacute; contra o choque de v&aacute;rios interesses dos grupos sociais e pol&iacute;ticos quando, na estrutura e na argamassa desses grupos, a sua for&ccedil;a de coes&atilde;o for menor que a dos danos que acarreta a perturba&ccedil;&atilde;o das incompatibilidades econ&ocirc;micas, financeiras e pol&iacute;ticas decorrentes desses atritos. A persist&ecirc;ncia, ou repeti&ccedil;&atilde;o cont&iacute;nua de tais perturba&ccedil;&otilde;es transforma o estado est&aacute;vel da comunidade para o estado inst&aacute;vel; transmuda a situa&ccedil;&atilde;o de confian&ccedil;a em expectativa de desconfian&ccedil;a, cria estados mentais opostos entre as intelig&ecirc;ncias condutoras, gerando, consequentemente, as incompatibilidades para a vida pol&iacute;tica em comum, dando origem ao div&oacute;rcio nacional.&quot;</i></b></p><p>Souza Lobo, <i>&quot;S&atilde;o Paulo na Federa&ccedil;&atilde;o&quot;</i>, p&aacute;g. 16.");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

