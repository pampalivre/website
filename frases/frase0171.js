<!--
document.write ("<p><b><i>&quot;... &Eacute; ali, pelo menos durante noventa minutos, nada mais existe. Nada mais importa. &Eacute; o seu time, suas cores, seu sangue correndo nas veias de 11 jogadores. De 11 jogadores correndo atr&aacute;s de uma bola. De 11 garotos sublimes ou guerreiros. Her&oacute;is ou bandidos. S&iacute;mbolo da mais suprema felicidade. Ou do mais dolorido fracasso.</i></p><p><i>Mas a 'massa' n&atilde;o quer saber desta dor. &Eacute; a festa do depois que lhe excita. &Eacute; a possibilidade de esquecer tantas dificuldades que lhes enleva a alma. Alma que ser&aacute; lavada com um t&iacute;tulo inesquec&iacute;vel. N&atilde;o importa como...&quot;</i></b></p><p>Publicado no jornal Zero Hora e citado por Roberto Ramos, no livro &quot;Futebol: Ideologia do Poder&quot;, Editora Vozes, 1984.</p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

