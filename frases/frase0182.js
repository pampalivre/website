<!--
document.write ("<p><b><i>&nbsp;&quot;Sant'Ana estava euf&oacute;rico no dia 9 de agosto. O disco havia vendido 20 mil c&oacute;pias. Isso era, absolutamente, extraordin&aacute;rio. Em apenas cinco dias, foram adquiridos 2/3 de toda a produ&ccedil;&atilde;o. O que representava uma m&eacute;dia di&aacute;ria de 4 mil discos vendidos. Sant'Ana tinha raz&otilde;es. Ele colaborara para esse &ecirc;xito. Gravara diversas chamadas para a R&aacute;dio Ga&uacute;cha e televis&atilde;o RBS. Nelas, conclamava o 'povo e a na&ccedil;&atilde;o tricolor' para adquiri-lo.</i></p><p><i>O futebol &eacute; um grande mercado. Ele se vende a partir do ingresso e das contribui&ccedil;&otilde;es dos torcedores. Isso significa apenas o pr&oacute;logo de seu car&aacute;ter mercantilista. Ele p&otilde;e na vitrina uma imensidade de subprodutos, que s&atilde;o identificados com os clubes e suas conquistas. O torcedor n&atilde;o passa de um consumidor.&quot;</i></p></b><p>Roberto Ramos, no livro &quot;Futebol: Ideologia do Poder&quot;, Editora Vozes, 1984.</p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

