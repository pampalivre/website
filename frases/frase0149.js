<!--
document.write ("<p><b><i>&quot; (... na novela) 'P&atilde;op&atilde;o Beijobeijo', de Walter Negr&atilde;o, 1983, &agrave;s seis. Ensinou o nordestino 'Sor&oacute;', Arnaud Rodrigues, a mexer nos computadores do Banco Eletr&ocirc;nico Ita&uacute;. Estranha ironia: um retirante, de uma das regi&otilde;es mais dramatizadas da geografia tropical, se envolvendo com alta tecnologia. Bela contradi&ccedil;&atilde;o.</i></b></p><p><b><i>&quot;Os ensinamentos n&atilde;o eram, exclusivamente, para 'Sor&oacute;'. Eles se destinavam para todos os telespectadores. O Ita&uacute; estava colocando a sua nova presta&ccedil;&atilde;o de servi&ccedil;o para eles. Um cursinho intensivo, disfar&ccedil;ado na hist&oacute;ria, para milh&otilde;es de brasileiros, que pensavam que viam mais um cap&iacute;tulo da novela.&quot;</i></b></p><p>Roberto Ramos, no livro<i> &quot;Gr&atilde;-Finos na Globo&quot;</i>, Editora Vozes, 1986, comentando o <i>merchandisign</i> nas novelas da Rede Globo.</p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

