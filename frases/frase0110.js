<!--
document.write ("<p><b><i>&quot;Certamente esses desejos (de autodetermina&ccedil;&atilde;o do sul) n&atilde;o dariam a sua presen&ccedil;a marcante se as coisas estivessem trilhando bons caminhos, o que lamentavelmente n&atilde;o tem acontecido. 'Arrumar' a pr&oacute;pria casa &eacute; bem mais f&aacute;cil do que arrumar um todo onde s&atilde;o por demais limitadas as inger&ecirc;ncias. E &eacute; isso, em s&iacute;ntese, o pensamento que toma corpo no Sul, o qual quer arrumar-se a s&iacute; mesmo desesperan&ccedil;ado de dias melhores e completamente descrente da classe pol&iacute;tica que tem infelicitado a na&ccedil;&atilde;o.&quot;</i></b></p><p>S&eacute;rgio Alves de Oliveira, no livro<i> &quot;Independ&ecirc;ncia do Sul&quot;</i>, Martins Livreiro Editor, 1986.</p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

