<!--
document.write ("<p><b><i>&quot;Para o telespectador, a novela representa uma possibilidade de fugir das amarguras do cotidiano e ir ao encontro de uma vida, cheia de mist&eacute;rio, suspense, amor e paix&atilde;o, onde tudo acaba bem. Os maus s&atilde;o castigados; os bons, recompensados. Isto ocasiona um tipo de fen&ocirc;meno psicol&oacute;gico chamado de </i></b><font color='red'><b><i>'satisfa&ccedil;&atilde;o substitutiva'</i></b></font><b><i>... Assistindo aos cap&iacute;tulos das novelas, as pessoas esquecem seus verdadeiros problemas&quot;...</i></b></p><p>Marques de Melo, pesquisador, citado por Roberto Ramos, no livro<i> &quot;Gr&atilde;-Finos na Globo&quot;</i>, Editora Vozes, 1986.</p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

