<!--
document.write ("<b><i>&quot;... descalabros emergem de todos os lados. S&oacute; &eacute; not&iacute;cia o que &eacute; deslize. O torto, o errado, o inusitado vencem o certo. Dignidade n&atilde;o chama a aten&ccedil;&atilde;o. A m&iacute;dia delira. A viol&ecirc;ncia nivela a cultura por baixo. Sem perspectivas e sem cren&ccedil;as, o povo banaliza o crime e se transforma num ente insens&iacute;vel. Um estado catat&ocirc;nico se instaura. Matar vira rotina. E morrer se transforma num evento cada vez mais sem choro. &Eacute; triste constatar que n&atilde;o somos, ainda, uma na&ccedil;&atilde;o.&quot;</i></b></p><p>Gaud&ecirc;ncio Torquato, analista pol&iacute;tico, em<i> &quot;N&atilde;o somos uma na&ccedil;&atilde;o&quot;,</i> jornal &quot;O Estado de S&atilde;o Paulo&quot;, 31/Ago/1993, p. 2.");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

