<!--
document.write ("<font color='red' size='4'><i>Digamos que tudo aquilo que sabes n�o seja 
apenas errado, mas uma mentira cuidadosamente engendrada. Digamos que tua mente 
esteja entupida de falsidades: sobre ti mesmo, sobre a hist�ria, sobre o mundo 
� tua volta, plantadas nela por for�as poderosas visando a conquistar, pacificamente, 
tua complac�ncia. A tua liberdade, nessas circunst�ncias, n�o passa de uma ilus�o, 
pois �s, na verdade, apenas um pe�o num grande enredo e o teu papel o de um 
cr�dulo indiferente. Isso se tiveres sorte. Se, em qualquer tempo, convier aos 
interesses de terceiros, o teu papel vai mudar: tua vida ser� destru�da, ser�s 
levado � fome e � mis�ria. Pode ser, at�, que tenhas de morrer. Quanto a isso, 
n�o h� nada que possa ser feito.</i></font></p>
<p><font color='red' size='4'><i>Ah! Se acontecer de conseguires descobrir um 
fiapo da verdade at� podes tentar alertar as pessoas, demolir, pela exposi��o, 
as bases dos que tramam nos bastidores. Mas, mesmo nesse caso, n�o ter�s muito 
mais a fazer. Eles s�o poderosos demais, invulner�veis demais, invis�veis demais, 
espertos demais. Da mesma forma que aconteceu com outros, antes de ti, tamb�m 
vais perder.</i></font></p>
<p><font color='red'>Charles P. Freund, editorialista do <i>The Washington Post</i>.</font></p>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->