<!--
document.write ("<b><i>&quot;Ningu&eacute;m, nem for&ccedil;a alguma, ser&aacute; capaz de deter a marcha dos acontecimentos e evitar esse desfecho. Os conflitos de interesses tender&atilde;o a agravar-se, porque a Uni&atilde;o ser&aacute; cada vez mais insaci&aacute;vel na arrecada&ccedil;&atilde;o de impostos, pois todo o dinheiro ser&aacute; pouco para custear sua gigantesca m&aacute;quina burocr&aacute;tica. A sangria que sofrer&atilde;o todos os Estados crescer&aacute; dia a dia, pois a sanguessuga estar&aacute; sempre e cada vez mais sedenta. Por isso, ao fim e ao cabo ter&aacute; de ser escolhido o caminho da separa&ccedil;&atilde;o ou o da exaust&atilde;o. E como sempre aconteceu com outros povos, em todos os tempos, os Estados optar&atilde;o pela segunda alternativa, evitando o suic&iacute;dio...&quot;</i></b></p><p>Jo&atilde;o Nascimento Franco, no livro <i>&quot;Fundamentos do Separatismo&quot;.</i>");
document.write ("</td></tr></table>");
document.write ("</center><br><br>");
 // -->

